<?php


//Returned position 0 -> true
var_dump(strpos("Hello, World!" , "Hello") >= 0);

//Something didn't exists, return false. False has his cast equals zero
var_dump(strpos("Hello, World!" , "Something") >= 0);

//Return position 7 -> true
var_dump(strpos("Hello, World!" , "World") != false);

//Return false -> true
var_dump(strpos("Hello, World!" , "Something") != false);

//Return 0 -> false. Zero has his cast equals false
var_dump(strpos("Hello, World!" , "Hello") != false);

//Return position 7 -> true
var_dump(strpos("Hello, World!" , "World") !== false);//Retorna a posição 7 -> true

//Return false -> true
var_dump(strpos("Hello, World!" , "Something") !== false);//Retorna false -> false

//Return 0 -> true. Zero , in this case, don't will be casted
var_dump(strpos("Hello, World!" , "Hello") !== false);//Retorna a posição 0 -> true