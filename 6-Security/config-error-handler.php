<?php

function myErrorHandler($errno, $errstr, $errfile, $errline, $errcontext)
{ ?>
    '<dl>
        <dt>$errno number/type of error:</dt>
        <dd><?php echo $errno; ?></dd>

        <dt>$errstr message of error:</dt>
        <dd><?php echo $errstr; ?></dd>

        <dt>$errfile file of error:</dt>
        <dd><?php echo $errfile; ?></dd>

        <dt>$errline line of error:</dt>
        <dd><?php echo $errline; ?></dd>

        <dt>$errcontext variables of error:</dt>
        <dd><?php var_dump($errcontext); ?></dd>
    </dl>'
<?php
}
set_error_handler("myErrorHandler");

trigger_error("Triggering an user notice", E_USER_NOTICE);

restore_error_handler();