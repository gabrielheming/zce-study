<?php

$int = 10;
$string = 'string';
$float = 10.5;
$boolean = true;
$null = null;
$array = ['a' , 'b' , 'c'];
$object = new stdClass;

$resource = fopen(__FILE__ , 'r');

printf('Is $int scalar? %s' , is_scalar($int) ? 'Yes' : 'No'); echo '<br />';

printf('Is $string scalar? %s' , is_scalar($string) ? 'Yes' : 'No'); echo '<br />';

printf('Is $float scalar? %s' , is_scalar($float) ? 'Yes' : 'No'); echo '<br />';

printf('Is $boolean scalar? %s' , is_scalar($boolean) ? 'Yes' : 'No'); echo '<br />';

printf('Is $null scalar? %s' , is_scalar($null) ? 'Yes' : 'No'); echo '<br />';

printf('Is $object scalar? %s' , is_scalar($object) ? 'Yes' : 'No'); echo '<br />';

printf('Is $array scalar? %s' , is_scalar($array) ? 'Yes' : 'No'); echo '<br />';

printf('Is $resource scalar? %s' , is_scalar($resource) ? 'Yes' : 'No'); echo '<br />';
