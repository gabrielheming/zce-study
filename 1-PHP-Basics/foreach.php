<?php

$somearray = array("hi", "this is a string", "this is a code");

foreach ($somearray as $row) {
    $row = strrev($row);
}

var_dump($somearray);

foreach ($somearray as $key => $row) {
    $somearray[$key] = strtoupper($row);
}

var_dump($somearray);

foreach ($somearray as &$row) {
    $row = strrev($row);
}

var_dump($somearray);