<?php
ini_set('error_reporting', E_ALL);
ini_set('log_errors' , TRUE);
ini_set('html_errors' , TRUE);
ini_set('display_errors' , TRUE); // production: FALSE, development: TRUE ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"  xml:lang="pt-br" lang="pt-br">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="pt-br" />

        <title>PHP ZCE Study</title>

       	<style>
            .result {
            	display: none;
            }
        </style>

        <script type="text/javascript" src="https://code.jquery.com/jquery-3.1.1.min.js" ></script>
        <script type="text/javascript">
        	$(function() {
                $('body .show').on('click' , function() {
					var $parent = $(this).closest('div');
					var $result = $parent.find('.result');

                    $result.show();
                    return false;
                });
            });
        </script>
    </head>
    <body>
        <h1>PHP ZCE Study</h1>

        <?php

        $directorys = new RecursiveIteratorIterator(new RecursiveDirectoryIterator(__DIR__));

        foreach($directorys AS $directory) {
            if(substr($directory , -4) === '.php' && $directory != __FILE__) {
                ob_start();

                require($directory);

                $result = ob_get_contents();
                ob_end_clean();

                printf(
                    '<div>
                        <h2>%s</h2>
                        <h3>Code</h3>
                        %s

                        <h3>Result</h3>
                        <a href="#" class="show">Show result</a><br /><br />
                        <div class="result">%s</div>

                    </div>
                    <hr />
                    ',
                    $directory,
                    highlight_string(file_get_contents($directory) , true),
                    $result
                );
            }
        }
        ?>
    </body>
</html>