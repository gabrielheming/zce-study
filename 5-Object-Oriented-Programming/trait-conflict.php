<?php

namespace Traits\Conflict;

trait A {

    public function doSomething()
    {
        return __TRAIT__;
    }
}

trait B {

    public function doSomething()
    {
        return __CLASS__;
    }
}

class C
{
    use A , B {
        A::doSomething insteadof B;
        B::doSomething as doSomethingElse;
    }
}

var_dump((new C)->doSomething());
var_dump((new C)->doSomethingElse());