<?php

namespace Foo\Bar;

trait A {
    public function doSomething()
    {
        echo "Class: ".__CLASS__;
    }

}

trait B {
    public function doSomething()
    {
        echo "Trait: ".__TRAIT__;
    }
}

trait C {
    public function doSomething()
    {
        echo "Namespace: ".__NAMESPACE__;
    }
}

class ClassA
{
    use A;
}

class ClassB
{
    use B;
}

class ClassC
{
    use C;
}

(new ClassA)->doSomething();
(new ClassB)->doSomething();
(new ClassC)->doSomething();