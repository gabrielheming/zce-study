<?php

class MyClass2Reflection
{
}

$reflection = new ReflectionClass('MyClass2Reflection');
var_dump($reflection->getFileName());
var_dump($reflection->isInternal());

$reflection = new ReflectionClass('PDO');
var_dump($reflection->getFileName());
var_dump($reflection->isInternal());

$reflection = new ReflectionExtension('PDO');
var_dump($reflection->getClasses());
var_dump($reflection->getClassNames());
var_dump($reflection->getDependencies());